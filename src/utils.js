const RGB_REGEX = /(rgb)\(([0-9]+),\s+([0-9]+),\s+([0-9]+)/;

export const Transparentize = (color, opacity) => color.replace(RGB_REGEX, 
  `rgba($2,$3,$4,${opacity}`);
