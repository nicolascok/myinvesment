import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import Chart from './Chart';
import Dataset from '../../data.json';

it('renders without crashing', () => {
  shallow(<Chart data={Dataset} riskTolerance={1} />);
});
