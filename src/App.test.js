import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import App from './App';

it('renders without crashing', () => {
  shallow(<App />);
});

it('updates riskTolerance on _onSliderChange', () => {
  const component = shallow(<App />);
  
  const riskTolerance = component.instance().state.riskTolerance;
  component.instance()._onSliderChange(2);

  const newRiskTolerance = component.instance().state.riskTolerance;
  expect(newRiskTolerance).not.toBe(riskTolerance);
});
