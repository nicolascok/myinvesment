import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import RiskSlider from './RiskSlider';
import Dataset from '../../data.json';

it('renders without crashing', () => {
  shallow(<RiskSlider onChange={jest.fn()} value={1} />);
});
