import React from 'react';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';
import './RiskSlider.css';

const RiskSlider = (props) => {
  return (
    <div className="Risk-level-slider">
      <h3 className="Risk-level-slider__title">Set you risk tolerance</h3>
      <InputRange maxValue={10} minValue={1} value={props.value}
        onChange={props.onChange} />
    </div>
  );
};

export default RiskSlider;
