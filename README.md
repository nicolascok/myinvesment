Demo here [https://build-ypawzfejmi.now.sh](https://build-ypawzfejmi.now.sh) !

# Investment portfolio Web App based on [React](https://reactjs.org/)

## Table of Contents

- [Running local environment](#running-local-environment)
  - [Dependencies](#dependencies)
  - [Setting up the Project](#setting-up-the-project)
  - [Run it!](#run-it)
- [Unit Testing](#unit-testing)
- [Technologies used](#technologies-used)

## Running local environment

### Dependencies
  
  You must have installed this dependencies before running any command.

  * [NodeJS](https://nodejs.org/en/) >= v9.0 (Follow the official instructions)
  
### Setting up the project
  
  Open terminal and run:

  * npm install

### Run it!

  In the terminal run:

  * npm start

  Now the project should be running in [localhost:3000](http://localhost:3000) or in the port specified by the last command's output.

## Unit Testing
  In order to run the unit test suites, open terminal and run:

  * npm test

## Technologies used
  
  * Javascript (EcmaScript 2015) with React as main Framework
  * CSS, HTML
  * Jest, Enzime (Unit testing)
