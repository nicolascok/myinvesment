import React, { Component } from 'react';
import Chart from './components/Chart/Chart';
import RiskSlider from './components/RiskSlider/RiskSlider';
import './App.css';

import Dataset from './data.json';

class App extends Component {
  constructor(props) {
    super(props);

    const riskTolerance = 1;

    this.state = {
      riskTolerance,
      dataset: Dataset
    };
    
    this._onSliderChange = this._onSliderChange.bind(this);
  }

  render() {
    const { riskTolerance, dataset } = this.state;

    return (
      <div className="App">
        <section className="App__risk-chart">
          <Chart data={dataset} riskTolerance={riskTolerance} />
        </section>
        <section className="App__risk-slider">
          <RiskSlider onChange={this._onSliderChange} value={riskTolerance} />
        </section>
      </div>
    );
  }

  _onSliderChange(value) {
    this.setState({
      riskTolerance: value
    });
  }
}

export default App;
