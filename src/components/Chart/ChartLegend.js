import React from 'react';
import './ChartLegend.css';

const ChartLegend = ({ data, riskTolerance }) => {
  const items = data.map(({ name, riskByLevel, color }, i) => (
    <li key={i} className="Risk-chart-legend__item">
      <span style={{ backgroundColor: color }}
        className="Risk-chart-legend__item__color-ref"></span>
      { name } 
      <span className="Risk-chart-legend__item__percentage">
        ({ riskByLevel[riskTolerance - 1] }%)
      </span>
    </li>
  ));

  return (
    <ul className="Risk-chart-legend">
      { items }
    </ul>
  );
};

export default ChartLegend;
