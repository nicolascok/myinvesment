import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import ChartLegend from './ChartLegend';
import Dataset from '../../data.json';

it('renders without crashing', () => {
  shallow(<ChartLegend data={Dataset} riskTolerance={1} />);
});
