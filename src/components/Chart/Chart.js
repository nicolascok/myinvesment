import React from 'react';
import { Doughnut } from 'react-chartjs-2';
import ChartLegend from './ChartLegend';
import { Transparentize } from '../../utils';

const options = {
  legend: {
    display: false
  }
};

const Chart = (props) => {
  const data = parseData(props.data, props.riskTolerance);

  return (
    <div className="Risk-chart">
      <Doughnut data={data} options={options} />
      <ChartLegend data={props.data} riskTolerance={props.riskTolerance} />
    </div>
  );
};

function parseData(data, riskTolerance) {
  const labels = [];
  const dataset = {
    data: [],
    backgroundColor: [],
    hoverBackgroundColor: [],
    label: 'My Investment',
  };
  
  data.forEach(({ name, riskByLevel, color }) => {
    labels.push(name);
    dataset.data.push(riskByLevel[riskTolerance - 1]);
    dataset.backgroundColor.push(Transparentize(color, .75));
    dataset.hoverBackgroundColor.push(color);
  });

  return {
    datasets: [ dataset ],
    labels
  }
}

export default Chart;
